Generic page
============

This is the type of the default home page. It suits very visual content, with many images and few text, like a welcome page, an advertisement page, or a project presentation for example.

On this page you can add 5 different types of blocks, in any order and number:

    .. image:: ../_static/images/generic_page_edit.png
        :alt:

    |

    - **Carousels**

        .. image:: ../_static/images/generic_page_carousel.png
            :alt: A carousel is a group of big images that can be scrolled through with side arrows

    - **Big cards**

        .. image:: ../_static/images/generic_page_big_cards(1).png
            :alt: Big cards consist of blocks of text next to an image

        .. image:: ../_static/images/generic_page_big_cards(2).png
            :alt: For several big cards, the image alternatively appears to the left and the right

    - **Small cards**

        .. image:: ../_static/images/generic_page_small_cards.png
            :alt: Small cards are blocks with text below an image, aligned by three

    - **Centered images**

        .. image:: ../_static/images/generic_page_centered_image.png
            :alt: The image is centered on the page, sized about 1/3 of the page width

    - **Centered text blocks**
    
        .. image:: ../_static/images/generic_page_centered_text.png
            :alt: The text block is centered on the page, sized about 1/3 of the page width


Read more about `inserting images <https://docs.wagtail.io/en/stable/editor_manual/new_pages/inserting_images.html>`_ and `using rich text fields <https://docs.wagtail.io/en/stable/editor_manual/new_pages/creating_body_content.html#rich-text-fields>`_.
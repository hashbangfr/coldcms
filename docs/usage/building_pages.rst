Building pages
==============

ColdCMS generates static websites. This means the pages aren't dynamically built, in order to reduce the server's jobs. Hence you will sometimes need to explicitely ask the server to re-build the pages, to make sure all your modifications have been taken into account on the client website.

Although, most of the time, the pages will re-build automatically when you save or publish your changes.

Sometimes, it is also possible that your browser cached the webpage. If you don't see the new page right away, try hitting `Ctrl + Shift + R`.

To re-build a page, click on the button **'More'**, and then **'Re-build page'** at the bottom of the dropdown menu.


- **From a parent page**:

.. image:: ../_static/images/re-build(3).png
    :alt: Button more on parent page, at the right of the menu

|

.. image:: ../_static/images/re-build(4).png
    :alt: Button re-build Page on parent page, at the bottom of the menu


- **From a child page**:

.. image:: ../_static/images/re-build(1).png
    :alt: Button more on child page, at the right of the menu

|

.. image:: ../_static/images/re-build(2).png
    :alt: Button re-build Page on child page, at the bottom of the menu

The page will take a few seconds to re-build. If you don't see your modifications right away on your website, wait a little longer and refresh the page.
Blog page
=========

The **first step** to create a blog is to give it a title and eventually an intro. The title is the one that will appear in the menu bar.

.. image:: ../_static/images/blog_index_edit.png
    :alt:

A blog index page is then created, listing all the blog posts (starting with the most recently posted) - here we suppose our blog already has two posts:

    .. image:: ../_static/images/blog_index_layout.png
        :alt: 

The **second step** is to add blog posts to your blog. A blog post is a type of page that can only be the child of a blog. Same way around, a blog can only have blog posts as children pages.

A blog post can have a title, a post date, tags, an intro, a content, and images.

.. image:: ../_static/images/blog_post_edit(1).png
    :alt:

.. image:: ../_static/images/blog_post_edit(2).png
    :alt:

.. image:: ../_static/images/blog_post_edit(3).png
    :alt:

More information about `inserting images <https://docs.wagtail.io/en/stable/editor_manual/new_pages/inserting_images.html>`_ and `using rich text fields <https://docs.wagtail.io/en/stable/editor_manual/new_pages/creating_body_content.html#rich-text-fields>`_.

A blog post looks like this:

    .. image:: ../_static/images/blog_post_layout.png
        :alt:

Additional pages exist aside from the main blog index page and the blog posts:

    - **One page per tag**, listing all posts with the given tag. E.g. in our example, there are two posts tagged 'mytag':

        .. image:: ../_static/images/blog_post_tag_index(1).png
            :alt:

        |

        And there is one post tagged 'othertag':

        .. image:: ../_static/images/blog_post_tag_index(2).png
            :alt:

        |

        A tag page appears when clicking on a tag from a blog post.

        .. image:: ../_static/images/blog_post_tag_click.png
            :alt:

    - **One page per author**, listing all posts written by the given author. E.g. in our example, there are two posts written by Pauline:

        .. image:: ../_static/images/blog_post_author_index(1).png
            :alt:

        .. image:: ../_static/images/blog_post_author_index(2).png
            :alt:

        |

        An author page appears when clicking on the author's name from a blog post.

        .. image:: ../_static/images/blog_post_author_click.png
            :alt:
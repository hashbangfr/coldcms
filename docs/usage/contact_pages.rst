Contact page
============

A contact page is specifically designed to contain contact information : address, phone number, email address, and opening hours.

.. image:: ../_static/images/contact_page_edit(1).png
    :alt:

.. image:: ../_static/images/contact_page_edit(2).png
    :alt:

.. image:: ../_static/images/contact_page_edit(3).png
    :alt:

.. image:: ../_static/images/contact_page_edit(4).png
    :alt:

Read more about `using rich text fields <https://docs.wagtail.io/en/stable/editor_manual/new_pages/creating_body_content.html#rich-text-fields>`_.

The contact page will look like this:

.. image:: ../_static/images/contact_page_layout.png
    :alt:
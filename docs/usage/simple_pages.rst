Simple page
===========

A simple page has a very basic layout: a title, and a text field, in which you can add images, videos or documents. Read more about rich text fields in `Wagtail's documentation <https://docs.wagtail.io/en/stable/editor_manual/new_pages/creating_body_content.html#rich-text-fields>`_.

It can be used for a legal notice, or any simple information display.

.. image:: ../_static/images/simple_page_edit.png
    :alt:

A simple page looks like:

.. image:: ../_static/images/simple_page_layout.png
    :alt:


Creating new pages
==================

See Wagtail documentation about `creating new pages <https://docs.wagtail.io/en/stable/editor_manual/new_pages/index.html>`_ and `editing existing pages <https://docs.wagtail.io/en/stable/editor_manual/editing_existing_pages.html>`_.

Page Types available in ColdCMS:

    .. toctree::
        :maxdepth: 2

        simple_pages
        generic_pages
        contact_pages
        faq_pages
        partners_pages
        blog_pages

General layout:

    - **Simple page**

        .. image:: ../_static/images/simple_page_layout.png
            :alt:

    - **Generic Page**

        .. image:: ../_static/images/generic_page_carousel.png
            :alt:

        .. image:: ../_static/images/generic_page_small_cards.png
            :alt:

        .. image:: ../_static/images/generic_page_big_cards(1).png
            :alt:

        .. image:: ../_static/images/generic_page_centered_image.png
            :alt:

        .. image:: ../_static/images/generic_page_centered_text.png
            :alt:

    - **Contact Page**

        .. image:: ../_static/images/contact_page_layout.png
            :alt:

    - **FAQ Page**

        .. image:: ../_static/images/faq_page_layout.png
            :alt:

    - **Partners Page**

        .. image:: ../_static/images/partners_page_layout.png
            :alt:

    - **Blog Page**

        .. image:: ../_static/images/blog_index_layout.png
            :alt:

        |

        .. image:: ../_static/images/blog_post_layout.png
            :alt:
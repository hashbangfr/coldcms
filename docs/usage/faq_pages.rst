FAQ page
========

A FAQ (Frequently Asked Questions) page is designed to easily add question categories, and under each one display several questions with their answers. The answers appear as collapsible content under the questions. The user can click on the arrow to the right of the question to show/hide the answer.

.. image:: ../_static/images/faq_page_edit(1).png
    :alt:

.. image:: ../_static/images/faq_page_edit(2).png
    :alt:

If you don't want to use categories, you can create only one category, without a name, and put all of your questions in there.

Read more about `using rich text fields <https://docs.wagtail.io/en/stable/editor_manual/new_pages/creating_body_content.html#rich-text-fields>`_.

The FAQ page looks like this:

.. image:: ../_static/images/faq_page_layout.png
    :alt:
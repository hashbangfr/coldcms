Customizing CSS
===============

If you know CSS, you can custom the appearance of your website yourself, by uploading a stylesheet.
For that, go into the menu **'Settings'**, then **'CSS stylesheet'**.

.. image:: ../_static/images/css_add(1).png
    :alt:

Then upload the file you wish to use as CSS, and save.
Read more about uploading documents on `Wagtail's documentation <https://docs.wagtail.io/en/stable/editor_manual/documents_images_snippets/documents.html>`_.

.. image:: ../_static/images/css_edit.png
    :alt:

For example, the default menu bar looks like this:

.. image:: ../_static/images/css_edit_before.png
    :alt:

After adding this CSS stylsheet:

.. code-block:: css

    .navbar-item{
        color: #9b5200;
    }

    a.navbar-item:hover{
        color: #2c25ff;
        background-color: #C1F742;
    }

The menu items have different colors:

.. image:: ../_static/images/css_edit_after.png
    :alt:

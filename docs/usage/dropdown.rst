Create a dropdown menu
======================

A dropdown menu can only be created as a child of a generic page (which is the default home page type).

.. image:: ../_static/images/dropdown_menu(1).png
    :alt: Dropdown editing interface has a title and a 'Show in menus' checkbox

Once you have created a dropdown menu, you can add as many child pages as menu items you want to appear in the dropdown. Each item links to the given child page. An item can also be an external or internal link.

In the end, a dropdown menu will look like this:

.. image:: ../_static/images/dropdown_menu(2).png
    :alt: When clicking on a dropdown menu, a list of links appears under it







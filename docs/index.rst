.. ColdCMS documentation master file, created by
   sphinx-quickstart on Thu May 14 14:33:56 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ColdCMS user documentation!
======================================

ColdCMS is mostly based on Wagtail.
You can find most of the information on how to create Pages, how to use Images, Documents and site Settings, in `Wagtail documentation (Editor's guide) <https://docs.wagtail.io/en/stable/editor_manual/index.html>`_.
Keep in mind that some features of Wagtail's admin don't exist in ColdCMS.

Before going into more specific documentation, please read `Wagtail's documentation about finding your way around <https://docs.wagtail.io/en/stable/editor_manual/finding_your_way_around/index.html>`_.

Then you can go through ColdCMS-specific documentation:

.. toctree::
   :maxdepth: 2

   usage/creating_pages
   usage/menu
   usage/building_pages
   usage/logo
   usage/footer
   usage/custom_css

from os.path import abspath, dirname, join

from attr import dataclass
from coldcms.wagtail_customization.templatetags import image_filters, list_filters

FILES_DIR = join(dirname(abspath(__file__)), "files")


@dataclass
class RenditionMock:
    url: str
    width: int


@dataclass
class FileMock:
    path: str


@dataclass
class ImageMock:
    title: str
    url: str
    width: int
    file: FileMock
    c = -1

    @property
    def default_alt_text(self):
        return self.title

    def get_rendition(self, _):
        self.c += 1
        self.c %= 6
        return RenditionMock(
            url=self.url.format(self.c), width=self.width * self.c
        )


def test_responsive_image():
    """ Test responsive_image tag """
    img_path = join(FILES_DIR, "rgb.png")
    img = ImageMock(
        title="title",
        url="http://example.com/i{}.png",
        width=300,
        file=FileMock(path=img_path),
    )
    res = image_filters.responsive_image(img)
    assert (
        res == "<img "
        'src="http://example.com/i0.png" '
        'alt="title" '
        'srcset="http://example.com/i1.png 300w, '
        "http://example.com/i2.png 600w, "
        "http://example.com/i3.png 900w, "
        "http://example.com/i4.png 1200w, "
        'http://example.com/i5.png 1500w">'
    )


def test_responsive_image_classes():
    """ Test responsive_image tag """
    img_path = join(FILES_DIR, "rgba.png")
    img = ImageMock(
        title="title",
        url="http://example.com/i{}.png",
        width=300,
        file=FileMock(path=img_path),
    )
    res = image_filters.responsive_image(img, classes="hello")
    assert (
        res == "<img "
        'class="hello" '
        'src="http://example.com/i0.png" '
        'alt="title" '
        'srcset="http://example.com/i1.png 300w, '
        "http://example.com/i2.png 600w, "
        "http://example.com/i3.png 900w, "
        "http://example.com/i4.png 1200w, "
        'http://example.com/i5.png 1500w">'
    )


def test_list_filters():
    list_test = [1, 2, 3, 4, 5 , 6]
    res = list_filters.index(list_test, 3)
    assert res == 4

import io
import os
import tempfile

import pytest
from coldcms.generic_page.models import GenericPage
from django.core import management
from wagtail.images.models import Image

pytestmark = pytest.mark.django_db


def test_setup_initial_data():
    assert GenericPage.objects.count() == 0
    assert Image.objects.count() == 0
    management.call_command("setup_initial_data", "--noconfirm")
    assert GenericPage.objects.count() == 1
    assert Image.objects.count() == 1


def test_setup_initial_data_already_existing_data():
    assert GenericPage.objects.count() == 0
    assert Image.objects.count() == 0
    management.call_command("setup_initial_data", "--noconfirm")
    assert GenericPage.objects.count() == 1
    assert Image.objects.count() == 1
    management.call_command("setup_initial_data", "--noconfirm")
    assert GenericPage.objects.count() == 1
    assert Image.objects.count() == 1


def test_setup_initial_data_stdin(monkeypatch):
    assert GenericPage.objects.count() == 0
    assert Image.objects.count() == 0
    monkeypatch.setattr("sys.stdin", io.StringIO("y"))
    management.call_command("setup_initial_data")
    assert GenericPage.objects.count() == 1
    assert Image.objects.count() == 1


def test_setup_initial_data_stdin_cancel(monkeypatch):
    assert GenericPage.objects.count() == 0
    assert Image.objects.count() == 0
    monkeypatch.setattr("sys.stdin", io.StringIO("n"))
    management.call_command("setup_initial_data")
    assert GenericPage.objects.count() == 0
    assert Image.objects.count() == 0


def test_setup_initial_data_language_fr(monkeypatch):
    monkeypatch.setenv("LANG", "fr_FR")
    management.call_command("setup_initial_data", "--noconfirm")
    home = GenericPage.objects.first()
    home_title = home.content_blocks[0].value.get("slides")[0].get("title")
    assert home_title == "Bienvenue sur votre instance ColdCMS"


def test_setup_initial_data_language_en(monkeypatch):
    monkeypatch.setenv("LANG", "en_EN")
    management.call_command("setup_initial_data", "--noconfirm")
    home = GenericPage.objects.first()
    home_title = home.content_blocks[0].value.get("slides")[0].get("title")
    assert home_title == "Welcome to your ColdCMS instance"


def test_collecttheme():
    with tempfile.TemporaryDirectory() as tmp_directory:
        management.call_command("collecttheme", "theme", "--directory", tmp_directory)
        assert os.path.exists(os.path.join(tmp_directory, "theme"))
        assert os.path.exists(os.path.join(tmp_directory, "theme", "static"))
        assert os.path.exists(os.path.join(tmp_directory, "theme", "static", "scss"))
        assert os.path.exists(os.path.join(tmp_directory, "theme", "static", "svg"))
        assert os.path.exists(os.path.join(tmp_directory, "theme", "templates"))


def test_collecttheme_directory_dont_exist():
    err = io.StringIO()
    management.call_command("collecttheme", "theme", "--directory", "notRealDirectoryIThink", stderr=err)
    assert err.getvalue() == "Error, directory notRealDirectoryIThink don't exist\n"


def test_collecttheme_theme_already_exist():
    with tempfile.TemporaryDirectory() as tmp_directory:
        management.call_command("collecttheme", "theme", "--directory", tmp_directory)
        err = io.StringIO()
        management.call_command("collecttheme", "theme", "--directory", tmp_directory, stderr=err)
        assert err.getvalue() == "Error : This theme already has files and directories\n"

import os
import subprocess
import tempfile
from concurrent.futures.thread import ThreadPoolExecutor

import coldcms.wagtail_customization.views
import pytest
import wagtailbakery.views
from coldcms.generic_page.models import GenericPage
from coldcms.site_settings.models import CSSStyleSheet
from coldcms.wagtail_customization import admin_views
from django.conf import settings
from django.db.models import signals
from django.urls import reverse
from wagtail.core.models import Site
from wagtail.documents.models import Document
from wagtail.images.models import Image

pytestmark = pytest.mark.django_db


call_count = 0


def fake_method(*args, **kwargs):
    global call_count
    call_count += 1


def fake_getsize(path):
    return 3615


class AllPublishedPagesView:
    build_method = fake_method

    def get_queryset(self):
        queryset = GenericPage.objects.all()
        if queryset:
            return queryset
        return []


class SitemapTemplateView:
    build_method = fake_method


class RobotsListView:
    build_method = fake_method


def noop(_=None, *args):
    pass


def raise_exception(*_, **kwargs):
    raise Exception("Dummy")


def test_generate_statics(client, admin_client, monkeypatch):
    monkeypatch.setattr(admin_views, "delete_cached_files", noop)
    monkeypatch.setattr(admin_views, "build_static_pages_async", noop)
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    Site.objects.create(root_page=home)
    resp = client.post(reverse("generate_statics", args=[home.pk]))
    assert resp.status_code == 302
    assert "/admin/login/" in resp.url
    resp = admin_client.post(
        reverse("generate_statics", args=[home.pk]),
        {},
        HTTP_ORIGIN='http://localhost'
    )
    assert resp.status_code == 302
    assert resp.url == reverse("wagtailadmin_explore", args=[home.pk])


def test_generate_statics_error(admin_client, monkeypatch):
    monkeypatch.setattr(admin_views, "delete_cached_files", raise_exception)
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    resp = admin_client.post(reverse("generate_statics", args=[home.pk]))
    assert resp.status_code == 302
    assert resp.url == reverse("wagtailadmin_explore", args=[home.pk])


def test_generate_statics_confirm(admin_client):
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    resp = admin_client.get(reverse("generate_statics", args=[home.pk]))
    assert resp.status_code == 200
    assert resp.context["page"].pk == home.pk


def test_delete_cached_files():
    with tempfile.TemporaryDirectory() as temp_dir:
        settings.BUILD_DIR = temp_dir
        html_cached_file = os.path.join(temp_dir, "index.html")
        with open(html_cached_file, "w") as f:
            f.write("")
        assert os.path.exists(html_cached_file)
        admin_views.delete_cached_files()
        assert not os.path.exists(html_cached_file)


def test_handle_publish(monkeypatch):
    global call_count

    monkeypatch.setattr(admin_views, "build_static_pages_async", fake_method)
    admin_views.handle_publish()
    assert call_count == 1
    call_count = 0


def test_handle_after_move_page(monkeypatch):
    global call_count

    monkeypatch.setattr(admin_views, "build_static_pages_async", fake_method)
    admin_views.handle_after_move_page(None, None)
    assert call_count == 1
    call_count = 0


def test_handle_unpublish(monkeypatch):
    global call_count

    monkeypatch.setattr(admin_views, "build_static_pages_async", fake_method)
    monkeypatch.setattr(admin_views, "delete_cached_files", fake_method)
    admin_views.handle_unpublish()
    assert call_count == 2
    call_count = 0


def test_minify_css_errors(monkeypatch):
    """
    Test that minify_css creates app.css even if subprocesses raise exceptions
    """
    page_paths = []
    monkeypatch.setattr(subprocess, "check_output", raise_exception)

    with tempfile.TemporaryDirectory() as temp_dir:
        app_css_path = os.path.join(temp_dir, "app.css")
        if os.path.exists(app_css_path):
            os.remove(app_css_path)
        original_app_css = app_css_path + ".original.css"
        with open(original_app_css, "w") as f:
            f.write("")
        monkeypatch.setattr("coldcms.assets.ORIGINAL_CSS_PATH", original_app_css)
        assert not os.path.exists(app_css_path)
        admin_views.minify_css(page_paths, app_css_path)
        assert os.path.exists(app_css_path)


def test_minify_css_ok(monkeypatch):
    """
    Test that minify_css creates app.css
    """
    page_paths = []
    monkeypatch.setattr(subprocess, "check_output", noop)
    monkeypatch.setattr(os.path, "getsize", fake_getsize)

    with tempfile.TemporaryDirectory() as temp_dir:
        app_css_path = os.path.join(temp_dir, "app.css")
        if os.path.exists(app_css_path):
            os.remove(app_css_path)
        with open(app_css_path + ".original.css", "w") as f:
            f.write("")
        original_app_css = app_css_path + ".original.css"
        monkeypatch.setattr("coldcms.assets.ORIGINAL_CSS_PATH", original_app_css)
        admin_views.minify_css(page_paths, app_css_path)
        assert os.path.exists(app_css_path)


def test_append_custom_CSS():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    site = Site.objects.create(root_page=home)
    custom_css = CSSStyleSheet.objects.create(site=site)

    with tempfile.TemporaryDirectory(dir=settings.MEDIA_ROOT) as temp_dir:
        custom_css_path = os.path.join(temp_dir, "custom.css")
        app_css_path = os.path.join(temp_dir, "app.css")
        with open(app_css_path, "w") as f:
            f.write("")
        with open(custom_css_path, "w") as f:
            f.write("custom")

        admin_views.append_custom_css(app_css_path)

        with open(app_css_path) as f:
            css_content = f.read()
        assert css_content == ""

        doc = Document.objects.create(title="custom.css", file=custom_css_path)
        custom_css.CSS_stylesheet = doc
        custom_css.save()

        admin_views.append_custom_css(app_css_path)

        with open(app_css_path) as f:
            css_content = f.read()
        assert css_content == "custom"


def test_minify_html():
    with tempfile.TemporaryDirectory() as temp_dir:
        html_path = os.path.join(temp_dir, "index.html")
        with open(html_path, "w") as f:
            f.write(
                "<html><head></head><body>Coucou !  Vous allez "
                "<span >Bien ?  </span></body></html>"
            )
        admin_views.minify_html([html_path])
        with open(html_path) as f:
            html_content = f.read()
        assert (
            html_content == "<html><head></head><body>Coucou ! Vous allez "
            "<span>Bien ?</span></body></html>"
        )


def test_minify_html_css(monkeypatch):
    global call_count

    monkeypatch.setattr(admin_views, "minify_css", fake_method)
    monkeypatch.setattr(admin_views, "minify_html", fake_method)
    admin_views.minify_html_css([], "")

    assert call_count == 2
    call_count = 0


def test_build_static_pages(monkeypatch):
    global call_count

    monkeypatch.setattr(admin_views, "minify_html_css", fake_method)
    monkeypatch.setattr(
        wagtailbakery.views, "AllPublishedPagesView", AllPublishedPagesView
    )
    admin_views.build_static_pages()

    assert call_count == 1
    call_count = 0


def test_build_static_pages_async(monkeypatch):
    global call_count

    monkeypatch.setattr(admin_views, "build_static_pages", fake_method)
    admin_views.build_static_pages_async()

    admin_views.executor.shutdown()
    admin_views.executor = ThreadPoolExecutor(max_workers=1)

    assert call_count == 1
    call_count = 0


def test_build_sitemap_robots(monkeypatch):
    global call_count
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    Site.objects.create(root_page=home)
    monkeypatch.setattr(
        wagtailbakery.views, "AllPublishedPagesView", AllPublishedPagesView
    )
    monkeypatch.setattr(
        coldcms.wagtail_customization.views, "SitemapTemplateView", SitemapTemplateView
    )
    monkeypatch.setattr(
        coldcms.wagtail_customization.views, "RobotsListView", RobotsListView
    )
    admin_views.build_static_pages()
    assert call_count == 3
    call_count = 0


def test_sitemap_view(client):
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    Site.objects.create(root_page=home)
    res = client.get(reverse("sitemap"))
    assert res.status_code == 200


def test_robot_view(client):
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    Site.objects.create(root_page=home)
    res = client.get(reverse("robots"))
    assert res.status_code == 200


def test_sitemap_view_request_server_name(client):
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    Site.objects.create(root_page=home, hostname="127.0.0.1")
    req = coldcms.wagtail_customization.views.SitemapTemplateView().create_request('/sitemap.xml')
    assert req.get_host() == "127.0.0.1"


def test_robot_view_request_server_name(client):
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    Site.objects.create(root_page=home, hostname="127.0.0.1")
    req = coldcms.wagtail_customization.views.RobotsListView().create_request('/robots.txt')
    assert req.get_host() == "127.0.0.1"


@pytest.mark.enable_signals
def test_site_saved_calls_statics_build(monkeypatch):
    global call_count

    monkeypatch.setattr(admin_views, "handle_publish", fake_method)

    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    site = Site(root_page=home)

    assert call_count == 0
    site.save()
    assert call_count == 1
    call_count = 0


@pytest.mark.enable_signals
def test_site_deleted_calls_statics_build(monkeypatch):
    global call_count

    monkeypatch.setattr(admin_views, "handle_publish", fake_method)

    assert call_count == 0
    signals.post_delete.has_listeners(Site)
    Site.objects.first().delete()
    assert call_count == 1
    call_count = 0


@pytest.mark.enable_signals
def test_image_deleted_calls_statics_build(monkeypatch):
    global call_count

    monkeypatch.setattr(admin_views, "handle_publish", fake_method)

    image = Image.objects.create(width="42", height="42")

    assert call_count == 0
    image.delete()
    assert call_count == 1
    call_count = 0

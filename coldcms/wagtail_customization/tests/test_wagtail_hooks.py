import re

import pytest
from coldcms.wagtail_customization.wagtail_hooks import (
    editor_css,
    editor_js,
    register_button_regenerate_statics,
)

pytestmark = pytest.mark.django_db


def test_register_statics_menu_item():
    class MockPage:
        pk = 1

    menu_item = register_button_regenerate_statics(MockPage(), None)
    assert 'href="/admin/generate_statics' in str(list(menu_item)[0])


def test_insert_editor_css():
    html = editor_css()
    assert re.match(
        r'<link rel="stylesheet" href="/static/css/coldcms_admin.\w+.css">',
        html,
    )


def test_insert_editor_js():
    html = editor_js()
    assert re.match(
        r'<script src="/static/js/coldcms_admin.\w+.js"></script>',
        html,
    )

import pytest
from coldcms.generic_page.models import GenericPage

pytestmark = pytest.mark.django_db


def test_full_clean():  # method in ColdCMSPageMixin
    special_chars = "áàâäÁÀÂÄéèêëÉÈÊËíìîïÍÌÏÎóòôöÓÒÔÖúùûüÙÚÛÜñÑçÇ"
    pagetest = GenericPage.objects.create(
        title=special_chars, path="/path/", depth=666,
    )
    for spe_char in special_chars:
        assert spe_char not in pagetest.slug

# Generated by Django 3.1.7 on 2021-05-03 11:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_auto_20210223_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='keywords',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Key words'),
        ),
    ]

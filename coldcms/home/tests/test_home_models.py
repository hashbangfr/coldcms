import pytest
from coldcms.blocks.models import DropDownPage
from django.http import Http404

pytestmark = pytest.mark.django_db


def test_dropdown_page():
    dropdown = DropDownPage()
    assert dropdown.url == "#"
    assert dropdown.show_in_menus
    assert dropdown.get_sitemap_urls() == []
    with pytest.raises(Http404) as _:
        dropdown.serve(None)

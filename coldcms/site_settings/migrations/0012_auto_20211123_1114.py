# Generated by Django 3.1.7 on 2021-11-23 11:14

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('site_settings', '0011_auto_20210413_0800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='footer',
            name='icons',
            field=wagtail.core.fields.StreamField([('social_media_icon', wagtail.core.blocks.StructBlock([('icon_type', wagtail.core.blocks.ChoiceBlock(blank=False, choices=[('facebook', 'Facebook'), ('twitter', 'Twitter'), ('github', 'GitHub'), ('gitlab', 'GitLab'), ('linkedin', 'LinkedIn'), ('instagram', 'Instagram'), ('youtube', 'YouTube'), ('pinterest', 'Pinterest'), ('tumblr', 'Tumblr')], help_text='Which social media is the URL linking to (will display the corresponding icon)', icon='site', label='Icon type')), ('link', wagtail.core.blocks.URLBlock(help_text='URL to your social media', icon='link', label='Link')), ('link_text', wagtail.core.blocks.CharBlock(help_text='The text displayed next to the social media icon. Should be of the type "Follow us on ...". Be aware that some social media companies require that this text appears next to their logo. Make sure by checking their branding website.', label='Link text', max_length=40, required=False)), ('blank', wagtail.core.blocks.BooleanBlock(label='Open link in a new tab', required=False))]))], blank=True, null=True, verbose_name='Social media icons'),
        ),
        migrations.AlterField(
            model_name='menuoptions',
            name='custom_menu_items',
            field=wagtail.core.fields.StreamField([('social_media_icon', wagtail.core.blocks.StructBlock([('icon_type', wagtail.core.blocks.ChoiceBlock(blank=False, choices=[('facebook', 'Facebook'), ('twitter', 'Twitter'), ('github', 'GitHub'), ('gitlab', 'GitLab'), ('linkedin', 'LinkedIn'), ('instagram', 'Instagram'), ('youtube', 'YouTube'), ('pinterest', 'Pinterest'), ('tumblr', 'Tumblr')], help_text='Which social media is the URL linking to (will display the corresponding icon)', icon='site', label='Icon type')), ('link', wagtail.core.blocks.URLBlock(help_text='URL to your social media', icon='link', label='Link')), ('link_text', wagtail.core.blocks.CharBlock(help_text='The text displayed next to the social media icon. Should be of the type "Follow us on ...". Be aware that some social media companies require that this text appears next to their logo. Make sure by checking their branding website.', label='Link text', max_length=40, required=False)), ('blank', wagtail.core.blocks.BooleanBlock(label='Open link in a new tab', required=False))])), ('link', wagtail.core.blocks.StructBlock([('link', wagtail.core.blocks.URLBlock(help_text='URL', icon='link', label='Link')), ('link_text', wagtail.core.blocks.CharBlock(label='Link text', max_length=40, required=False))]))], blank=True, null=True, verbose_name='Links and social media icons'),
        ),
    ]

# Generated by Django 3.1.7 on 2021-04-30 13:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20190725_1326'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogindexpage',
            name='keywords',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Key words'),
        ),
    ]

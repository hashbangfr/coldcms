from datetime import datetime
from os.path import abspath, dirname, join

import pytest
from coldcms.blog.models import (
    BlogAuthorIndexPage,
    BlogDateIndexPage,
    BlogIndexPage,
    BlogListAuthorsIndexPage,
    BlogListDatesIndexPage,
    BlogListTagsIndexPage,
    BlogPage,
    BlogPageGalleryImage,
    BlogPageTag,
    BlogTagGroupByBlogPage,
    BlogTagIndexPage,
)
from coldcms.generic_page.models import GenericPage
from django.contrib.auth.models import User
from django.core.files.images import ImageFile
from django.utils.text import slugify
from taggit.models import Tag
from wagtail.core.models import Site
from wagtail.images.models import Image as WagtailImage

pytestmark = pytest.mark.django_db
FILES_DIR = join(dirname(abspath(__file__)), "files")


def test_blog_index_get_context(client):
    homepage = GenericPage.objects.create(title="Home", path="0003", depth=1, numchild=1)
    Site.objects.all().update(root_page=homepage)
    blog = BlogIndexPage.objects.create(title="Blog", path="00030001", depth=2, numchild=2)
    BlogPage.objects.create(
        title="My post1",
        path="000300010001",
        depth=3,
        date=datetime.utcnow(),
        body="so much contents",
    )
    blog_page_2 = BlogPage.objects.create(
        title="My post2",
        path="000300010002",
        depth=3,
        date=datetime.utcnow(),
        body="so much contents",
        live=False,
    )

    resp = client.get(blog.url)
    assert len(resp.context["blog_pages"]) == 1
    blog_page_2.live = True
    blog_page_2.save()
    resp = client.get(blog.url)
    assert len(resp.context["blog_pages"]) == 2


def test_blog_page_main_image():
    blog_page = BlogPage.objects.create(
        title="My post1",
        path="0002",
        depth=1,
        date=datetime.utcnow(),
        body="so much contents",
    )
    image_file = ImageFile(open(join(FILES_DIR, "test.png"), "rb"), name="test.png")
    image = WagtailImage.objects.create(title="title", file=image_file)

    assert blog_page.main_image() is None
    BlogPageGalleryImage.objects.create(page=blog_page, image=image)
    assert blog_page.main_image().pk == image.pk


def test_blog_page_context(client):
    user = User.objects.create(username="blablabla")
    blog_page = BlogPage.objects.create(
        title="My post1",
        path="0002",
        depth=1,
        date=datetime.utcnow(),
        body="so much contents",
        owner=user,
    )
    Site.objects.all().update(root_page=blog_page)
    resp = client.get(blog_page.url)
    assert resp.context["tag_page"] == {}
    assert resp.context["author_index"] is None

    tag = Tag.objects.create(name="test tag")
    BlogPageTag.objects.create(tag=tag, content_object=blog_page)
    tag_index = BlogTagIndexPage.objects.create(
        title="Blog index", path="0004", depth=1, tag=tag
    )
    tag_page = {tag_index.tag: tag_index.url}
    author_index = BlogAuthorIndexPage.objects.create(
        title="Blog index", path="0006", depth=1, author=user
    )

    resp = client.get(blog_page.url)
    assert resp.context["tag_page"] == tag_page
    assert resp.context["author_index"] == author_index


def test_blog_tag_by_blog_page_context(client):
    tag = Tag.objects.create(name="test tag")
    blog = BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0002"
    )
    Site.objects.all().update(root_page=blog)
    blog_page = BlogPage(
        title="My post1",
        date=datetime.utcnow(),
        body="so much contents",
    )
    blog.add_child(instance=blog_page)
    blog_page.save_revision().publish()
    BlogPageTag.objects.create(tag=tag, content_object=blog_page)
    blog_tag_by_blog_page = BlogTagGroupByBlogPage(
        title=f"Tag {tag.name} group by blog {blog.title}",
        tag=tag,
        slug=slugify(tag.name)
    )
    blog.add_child(instance=blog_tag_by_blog_page)
    blog_tag_by_blog_page.save_revision().publish()
    resp = client.get(blog_tag_by_blog_page.url)
    assert resp.context["blog_pages"][0] == blog_page
    assert resp.context["blog"] == blog


def test_blog_tag_index_page_context(client):
    tag = Tag.objects.create(name="test tag")
    blog_tag_page = BlogTagIndexPage.objects.create(
        title="My post1", path="0002", depth=1, tag=tag
    )
    blog_page = BlogPage.objects.create(
        title="My post1",
        path="0003",
        depth=1,
        date=datetime.utcnow(),
        body="so much contents",
    )
    BlogPageTag.objects.create(tag=tag, content_object=blog_page)
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    Site.objects.all().update(root_page=blog_tag_page)
    resp = client.get(blog_tag_page.url)
    assert resp.context["blog_pages"][0] == blog_page


def test_blog_list_tags_index_page_context(client):
    tag = Tag.objects.create(name="test tag")
    blog_tag_page = BlogTagIndexPage.objects.create(
        title="My post1", path="0002", depth=1, tag=tag
    )
    list_tags_index = BlogListTagsIndexPage.objects.create(
        title="BlogListTagsIndexPage", depth=1, path="0003"
    )
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    Site.objects.all().update(root_page=list_tags_index)
    resp = client.get(list_tags_index.url)
    assert resp.context["tags_pages"][0] == blog_tag_page


def test_blog_author_index_page_context(client):
    user = User.objects.create(username="blablabla")
    blog_author_page = BlogAuthorIndexPage.objects.create(
        title="My post1", path="0002", depth=1, author=user
    )
    blog_page = BlogPage.objects.create(
        title="My post1",
        path="0003",
        depth=1,
        date=datetime.utcnow(),
        body="so much contents",
        owner=user,
    )
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    Site.objects.all().update(root_page=blog_author_page)
    resp = client.get(blog_author_page.url)
    assert resp.context["blog_pages"][0] == blog_page


def test_blog_list_authors_index_page_context(client):
    user = User.objects.create(username="blablabla")
    blog_author_page = BlogAuthorIndexPage.objects.create(
        title="My post1", path="0002", depth=1, author=user
    )
    list_authors_index = BlogListAuthorsIndexPage.objects.create(
        title="BlogListAuthorsIndexPage", depth=1, path="0003"
    )
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    Site.objects.all().update(root_page=list_authors_index)
    resp = client.get(list_authors_index.url)
    assert resp.context["authors_pages"][0] == blog_author_page


def test_blog_date_index_page_context(client):
    date = datetime.utcnow()
    blog_date_page = BlogDateIndexPage.objects.create(
        title="My post1", path="0002", depth=1, date=date
    )
    blog_page = BlogPage.objects.create(
        title="My post1", path="0003", depth=1, date=date, body="so much contents",
    )
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    Site.objects.all().update(root_page=blog_date_page)
    resp = client.get(blog_date_page.url)
    assert resp.context["blog_pages"][0] == blog_page


def test_blog_list_dates_index_page_context(client):
    date = datetime.utcnow()
    blog_date_page = BlogDateIndexPage.objects.create(
        title="My post1", path="0002", depth=1, date=date
    )
    list_dates_index = BlogListDatesIndexPage.objects.create(
        title="BlogListDatesIndexPage", depth=1, path="0003"
    )
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    Site.objects.all().update(root_page=list_dates_index)
    resp = client.get(list_dates_index.url)
    assert resp.context["dates_pages"][0] == blog_date_page

import json
from datetime import datetime

import pytest
from coldcms.blog.models import BlogIndexPage, BlogPage
from coldcms.generic_page.models import GenericPage
from wagtail.core.models import Site

pytestmark = pytest.mark.django_db


@pytest.fixture
def create_blog_pages():
    date = datetime.utcnow()
    site = Site.objects.first()
    home = GenericPage.objects.create(title="ColdCMS", path="00010002", depth=2)
    site.root_page = home
    site.save()
    root = site.root_page.get_root()
    root.numchild = 1
    root.save()

    blog_index_page = BlogIndexPage.objects.create(title="blog1", intro="intro", depth=3, path="000100020003")
    blog_page = BlogPage(
        title="My post1", date=date, body="so much contents"
    )
    blog_index_page.add_child(instance=blog_page)
    blog_page.save_revision().publish()

    blog_index_page2 = BlogIndexPage.objects.create(title="blog2", intro="intro", depth=3, path="000100020004")
    blog_page2 = BlogPage(
        title="My post 2", date=date, body="so much contents"
    )
    blog_index_page2.add_child(instance=blog_page2)
    blog_page2.save_revision().publish()
    return home, blog_index_page2


def test_blog_list_value__no_page_selected(client, create_blog_pages):
    home, blog_index_page2 = create_blog_pages

    home.content_blocks = json.dumps(
                [
                    {
                        "type": "blog_list_block",
                        "value": {
                            "title": "articles_list",
                            "number": 5,
                            "page": None,
                        }
                    }
                ]
            )

    assert home.content_blocks.get_prep_value()[0]['type'] == 'blog_list_block'
    home.save_revision().publish()
    assert home.content_blocks[0].value.blog_list.count() == 2


def test_blog_list_value__page_selected(client, create_blog_pages):
    home, blog_index_page2 = create_blog_pages
    home.content_blocks = json.dumps(
                [
                    {
                        "type": "blog_list_block",
                        "value": {
                            "title": "articles_list",
                            "number": 5,
                            "page": blog_index_page2.pk,
                        }
                    }
                ]
            )
    assert home.content_blocks.get_prep_value()[0]['type'] == 'blog_list_block'
    home.save_revision().publish()
    assert home.content_blocks[0].value.blog_list.count() == 1
    assert home.content_blocks[0].value.blog_list.first().title == "My post 2"

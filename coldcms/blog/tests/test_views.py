import datetime

import pytest
from coldcms.blog import views
from coldcms.blog.models import (
    BlogAuthorIndexPage,
    BlogDateIndexPage,
    BlogIndexPage,
    BlogListAuthorsIndexPage,
    BlogListDatesIndexPage,
    BlogListTagsIndexPage,
    BlogPage,
    BlogPageTag,
    BlogTagGroupByBlogPage,
    BlogTagIndexPage,
)
from coldcms.blog.views import (
    refresh_authors,
    refresh_dates,
    refresh_index_pages,
    refresh_tags,
    refresh_tags_groupby_blog,
    unpublish_before_delete,
)
from coldcms.generic_page.models import GenericPage
from django.contrib.auth.models import User
from factory.django import mute_signals
from taggit.models import Tag
from wagtail.core.signals import page_published, page_unpublished

pytestmark = pytest.mark.django_db

call_count = 0


def fake_refresh():
    global call_count
    call_count += 1


def fake_refresh_with_instance(instance=BlogPage):
    global call_count
    call_count += 1


def noop(*_):
    pass


def test_refresh_index_pages(monkeypatch):
    global call_count
    call_count = 0
    monkeypatch.setattr(views, "refresh_tags_groupby_blog", fake_refresh_with_instance)
    monkeypatch.setattr(views, "refresh_tags", fake_refresh)
    monkeypatch.setattr(views, "refresh_authors", fake_refresh)
    monkeypatch.setattr(views, "refresh_dates", fake_refresh)
    refresh_index_pages(BlogPage)
    assert call_count == 4
    call_count = 0


def test_refresh_dates(monkeypatch):
    root = GenericPage.get_first_root_node()
    homepage = GenericPage(title="Home")
    root.add_child(instance=homepage)
    root.get_first_child().delete()
    dates = [
        datetime.date(year=2019, month=month, day=1) for month in range(1, 13)
    ]
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=3, path="000100020004"
    )
    for i, date in enumerate(dates[4:]):
        BlogPage.objects.create(
            title="My post1",
            path=f"000100020004001{i+1}",
            depth=4,
            date=date,
            body="so much contents",
        )

    for i, date in enumerate(dates[:-4]):
        BlogDateIndexPage.objects.create(
            title="My post1", path=f"00010002002{i+1}", depth=3, date=date
        )

    assert BlogListDatesIndexPage.objects.first() is None
    with mute_signals(page_published, page_unpublished):
        refresh_dates()
    assert BlogListDatesIndexPage.objects.first() is not None
    assert BlogDateIndexPage.objects.count() == 8
    assert {
        index_page.date for index_page in BlogDateIndexPage.objects.all()
    } == set(dates[4:])


def test_refresh_authors(monkeypatch):
    root = GenericPage.get_first_root_node()
    homepage = GenericPage(title="Home")
    root.add_child(instance=homepage)
    root.get_first_child().delete()
    authors = [User.objects.create(username=f"user{i}") for i in range(1, 13)]
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    now = datetime.datetime.now()
    for i, author in enumerate(authors[4:]):
        BlogPage.objects.create(
            title="My post1",
            path=f"001{i+1}",
            depth=1,
            date=now,
            body="so much contents",
            owner=author,
        )

    for i, author in enumerate(authors[:-4]):
        BlogAuthorIndexPage.objects.create(
            title="My post1", path=f"002{i+1}", depth=1, author=author
        )

    assert BlogListAuthorsIndexPage.objects.first() is None
    with mute_signals(page_published, page_unpublished):
        refresh_authors()
    assert BlogListAuthorsIndexPage.objects.first() is not None
    assert BlogAuthorIndexPage.objects.count() == 8
    assert {
        index_page.author for index_page in BlogAuthorIndexPage.objects.all()
    } == set(authors[4:])


def test_refresh_tags(monkeypatch):
    root = GenericPage.get_first_root_node()
    homepage = GenericPage(title="Home")
    root.add_child(instance=homepage)
    root.get_first_child().delete()
    tags = [Tag.objects.create(name=f"tag{i}") for i in range(1, 13)]
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    now = datetime.datetime.now()
    for i, tag in enumerate(tags[4:]):
        blog_page = BlogPage.objects.create(
            title="My post1",
            path=f"001{i+1}",
            depth=1,
            date=now,
            body="so much contents",
        )
        BlogPageTag.objects.create(tag=tag, content_object=blog_page)

    for i, tag in enumerate(tags[:-4]):
        BlogTagIndexPage.objects.create(
            title="My post1", path=f"002{i+1}", depth=1, tag=tag
        )

    assert BlogListTagsIndexPage.objects.first() is None
    with mute_signals(page_published, page_unpublished):
        refresh_tags()
    assert BlogListTagsIndexPage.objects.first() is not None
    assert BlogTagIndexPage.objects.count() == 8
    assert {
        index_page.tag for index_page in BlogTagIndexPage.objects.all()
    } == set(tags[4:])


def test_refresh_tags_groupby_blog(monkeypatch):
    root = GenericPage.get_first_root_node()
    homepage = GenericPage(title="Home")
    root.add_child(instance=homepage)
    root.get_first_child().delete()
    tags = [Tag.objects.create(name=f"tag{i}") for i in range(1, 13)]
    blog = BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    now = datetime.datetime.now()
    for i, tag in enumerate(tags[4:]):
        blog_page = BlogPage(
            title="My post1",
            date=now,
            body="so much contents",
        )
        blog.add_child(instance=blog_page)
        blog_page.save_revision().publish()
        BlogPageTag.objects.create(tag=tag, content_object=blog_page)
        with mute_signals(page_published, page_unpublished):
            refresh_tags_groupby_blog(blog_page)
    assert BlogTagGroupByBlogPage.objects.count() == 8
    assert {
        index_page.tag for index_page in BlogTagGroupByBlogPage.objects.all()
    } == set(tags[4:])


def test_unpublish_blog_page(monkeypatch):
    root = GenericPage.get_first_root_node()
    homepage = GenericPage(title="Home")
    root.add_child(instance=homepage)
    root.get_first_child().delete()
    blog = BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    now = datetime.datetime.now()
    blog_page = BlogPage(
        title="My post1",
        date=now,
        body="so much contents",
    )
    blog.add_child(instance=blog_page)
    blog_page.save_revision().publish()
    assert blog_page.live is True
    with mute_signals(page_published, page_unpublished):
        unpublish_before_delete(instance=blog_page)
    assert blog_page.live is False

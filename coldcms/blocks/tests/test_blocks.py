import pytest
from coldcms.blocks.blocks import CTABlock, FormBlock
from coldcms.form_page.models import FormPage
from coldcms.generic_page.models import GenericPage

pytestmark = pytest.mark.django_db


def test_link_struct_value_external_url():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    cta = CTABlock()
    link_value = cta.to_python(
        {"text": "blablabla", "custom_url": "https://lalala.com", "page": home.pk,}
    )
    assert link_value.url() == "https://lalala.com"


def test_link_struct_value_page():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    home.url_path = "/home/"
    home.save()
    cta = CTABlock()
    link_value = cta.to_python({"text": "blablabla", "page": home.pk})
    assert link_value.url() == home.url


def test_link_struct_value_extra_url():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    cta = CTABlock()
    link_value = cta.to_python(
        {
            "text": "blablabla",
            "custom_url": "https://lalala.com/",
            "page": home.pk,
            "extra_url": "#blabla",
        }
    )
    assert link_value.url() == "https://lalala.com/#blabla"


def test_link_struct_value_no_url():
    cta = CTABlock()
    link_value = cta.to_python({"text": "blablabla", "extra_url": "#blabla"})
    assert link_value.url() == "#blabla"


def test_link_struct_value_text_page():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    cta = CTABlock()
    link_value = cta.to_python({"page": home.pk})
    assert link_value.text() == home.title


def test_link_struct_value_text_url():
    cta = CTABlock()
    link_value = cta.to_python({"custom_url": "https://lalala.com/"})
    assert link_value.text() == "https://lalala.com/"


def test_link_struct_value_text():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    cta = CTABlock()
    link_value = cta.to_python({"text": "blablabla", "page": home.pk})
    assert link_value.text() == "blablabla"


def test_form_struct_value_no_text():
    form_page = FormPage.objects.create(title="My Form", path="/path/", depth=666)
    form_block = FormBlock()
    form_value = form_block.to_python({"page": form_page.pk})
    assert form_value.text() == "My Form"


def test_form_struct_value_text():
    form_page = FormPage.objects.create(title="My Form", path="/path/", depth=666)
    form_block = FormBlock()
    form_value = form_block.to_python({"text": "title", "page": form_page.pk})
    assert form_value.text() == "title"


def test_form_struct_value_form():
    form_page = FormPage.objects.create(title="My Form", path="/path/", depth=666)
    form_block = FormBlock()
    form_value = form_block.to_python({"text": "title", "page": form_page.pk})
    assert form_value.form_page() == form_page

import pytest
from coldcms.contact.models import ContactPage

pytestmark = pytest.mark.django_db


def test_address(monkeypatch):
    contact_page = ContactPage.objects.create(
        title="Contact Us",
        path="/path/",
        depth=666,
        content="contact us!",
        address="2 Rue Professeur Zimmermann, 69007 Lyon",
    )
    contact_page.save()
    assert contact_page.address == "2 Rue Professeur Zimmermann, 69007 Lyon"
    assert contact_page.content == "contact us!"

# Generated by Django 3.1.7 on 2021-04-30 13:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('form_page', '0002_formfield_clean_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='formpage',
            name='keywords',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Key words'),
        ),
    ]

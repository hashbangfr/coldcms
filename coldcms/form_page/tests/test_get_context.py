import pytest
from coldcms.form_page.models import FormPage
from django.http import HttpRequest

pytestmark = pytest.mark.django_db


def test_get_context_form_page():
    form_page = FormPage()
    request = HttpRequest()
    request.META['HTTP_REFERER'] = 'previous'
    context_form_page = form_page.get_context(request)
    assert context_form_page['previous_url'] == 'previous'

BenchMark ColdCMS
=================

In this section we are often talking about `efficiency`, it's the percentage of bytes stripped out from the original file. So if a file weights 100Ko and after some optimizations it weights 80Ko the efficiency will be of `20%`

Css
---

Minify
......

Minification of Bulma.
Here, the goal is to see the efficiency of the minification of the BulmaCSS framework.

From Bulma To Bulma.min.css

Efficiency: **15.38 %**


Purge
.....

https://www.purgecss.com/

This tool removes unused CSS.

Efficiency: **39.51 %**


Clean
.....

https://github.com/jakubpawlowicz/clean-css

CleanCSS optimizes and minifies the CSS generated from the previous Purge step.

Efficiency: **86.16 %**

At the end the CSS of the website https://coldcms.hashbang.fr/ (which also contains custom CSS) weights 26Ko, knowing that bulma.css weights 221Ko, we stripped out `88%` of the data!

HTML
----

Minify
......

We minify the content of each HTML page.

Efficiency: Between **15 and 31 %** per page



Apache HTTP server benchmarking tool
------------------------------------

https://httpd.apache.org/docs/2.4/programs/ab.html

This is a tool for benchmarking your server. Our goal with this tool is to compare a website rendered by a uwsgi behind nginx (aka, a standard installation of Wagtail) versus a website rendered using the ColdCMS build system and served by nginx.

Protocol
........

Tools : 

- ab (Apache benchmarking)

Parameters : 
    
- Time 30s 
- Concurrency : 20

Command for each test :

.. code-block:: bash
    
    ab -t 30 -c 20 http://localhost:8888

Html size to transfer in request :    

- 11536 bytes

Condition :

For this tests, we are deploying a ColdCMS instance on a machine with:

- Intel(R) Core(TM) i5-5300U CPU @ 2.30GHz - 2 cpu cores (4 virtual cores)
- 16Go RAM DDR3, Memory Speed: 1600 MT/s
- Hard Disk SSD 256Go


First test : ColdCMS with Uwsgi without Django cache system.

- `configure Nginx with Uwsgi to connect the Django to Nginx. The configuration listen on localhost:8888 <nginx_1.conf>`_



Second test : ColdCMS with Uwsgi with Django cache system.

- install `wagtail-cache <https://docs.coderedcorp.com/wagtail-cache/stable/getting_started/install.html>`_ on ColdCMS and restart uwsgi service.

Third test : ColdCMS with static pages generated in build directory - Pages rendered by Nginx. **(This is the standard configuration)**

- `Change Nginx configuration <nginx_3.conf>`_
- Restart Uswgi and Nginx


=============================== =================== ================
Setup                           Requests per second Time per request
------------------------------- ------------------- ----------------
 Uwsgi - Without Cache          29.82               670.606 ms
 Uwsgi - With Django Cache      259.25              77.147 ms
 Nginx render static            59890.07            0.334 ms
=============================== =================== ================

Looking at this data, we can safely say that the performance improvement of ColdCMS over a standard Wagtail deployment is really significant.

